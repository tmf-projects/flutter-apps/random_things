# Changelog

#### Ver. 0.1.0+1 (2021/06/08)
- [ENG](#v0101eng)
- [RUS](#v0101rus)

## List

#### Ver. 0.1.0+1 (2021/06/08)
<p id="v0101eng">

- Added the "NumberScreen" screen"
- Preparing the interface for adding settings
- SnackBar appears if there is no specific screen yet
- Updates of the translation into Russian and English
- Change the name of the application package
- Adjust the content in the navigation menu
- Service adjustment
- Adjust app themes
</p>

#### Ver. 0.1.0+1 (2021/06/08)
<p id="v0101rus">

- Добавлен экран "NumberScreen"
- Подготовка интерфейса к добавлению настроек
- Появление SnackBar, если определённого экрана ещё нет
- Обновления перевода на Русский и Английский
- Изменение названия пакета приложения
- Корректировка содержимого в меню навигации
- Корректировка сервиса
- Корректировка тем приложения
</p>
