import 'package:anitex/anitex.dart';
import 'package:flutter/material.dart';
import 'package:random_things/generated/l10n.dart';
import 'package:random_things/screens/random/number_screen.dart';

class AnimText extends StatelessWidget {
  final String text;
  final TextStyle? style;
  final TextAlign textAlign;

  const AnimText(this.text,
      {Key? key, this.style, this.textAlign = TextAlign.left});

  @override
  Widget build(BuildContext context) {
    return AnimatedText(text, style: style, textAlign: textAlign);
  }
}

Widget header(BuildContext context, S s) {
  return Container(
    width: double.infinity,
    height: 175,
    padding: EdgeInsets.all(10),
    alignment: Alignment.center,
    decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
      Theme.of(context).primaryColor,
      Theme.of(context).scaffoldBackgroundColor
    ], begin: Alignment.topLeft, end: Alignment.bottomRight)),
    child: Column(
      children: [
        SizedBox(height: 50),
        Text(
          s.app_name,
          style: Theme.of(context).textTheme.headline6,
        ),
        Text(
          s.app_description,
          style: Theme.of(context).textTheme.subtitle1,
        ),
        SizedBox(height: 20),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Container(
                  padding: EdgeInsets.all(8),
                  child: Icon(Icons.settings_rounded)),
            )
          ],
        )
      ],
    ),
  );
}

Widget categoryList(BuildContext context, S s) {
  return ExpansionTile(
    title: Text(s.categories_text),
    children: [
      Padding(
        padding: EdgeInsets.only(left: 10),
        child: Column(
          children: [
            ListTile(
                title: Text(s.address_text),
                onTap: () {
                  snackBarInfo(context, s);
                }),
            //ListTile(title: Text(s.appliance_text)),
            //ListTile(title: Text(s.app_text)),
            //ListTile(title: Text(s.bank_text)),
            //ListTile(title: Text(s.beer_text)),
            //ListTile(title: Text(s.blood_text)),
            //ListTile(title: Text(s.business_credit_card_text)),
            //ListTile(title: Text(s.code_text)),
            ListTile(
                title: Text(s.coffee_text),
                onTap: () {
                  snackBarInfo(context, s);
                }),
            //ListTile(title: Text(s.commerce_text)),
            ListTile(
                title: Text(s.company_text),
                onTap: () {
                  snackBarInfo(context, s);
                }),
            //ListTile(title: Text(s.computer_text)),
            //ListTile(title: Text(s.crypto_text)),
            //ListTile(title: Text(s.crypto_coin_text)),
            ListTile(
                title: Text(s.color_text),
                onTap: () {
                  snackBarInfo(context, s);
                }),
            //ListTile(title: Text(s.dessert_text)),
            ListTile(
                title: Text(s.device_text),
                onTap: () {
                  snackBarInfo(context, s);
                }),
            ListTile(
                title: Text(s.food_text),
                onTap: () {
                  snackBarInfo(context, s);
                }),
            ListTile(
                title: Text(s.name_text),
                onTap: () {
                  snackBarInfo(context, s);
                }),
            //ListTile(title: Text(s.users_text)),
            //ListTile(title: Text(s.vehicle_text)),
            //ListTile(title: Text(s.id_number_text)),
            //ListTile(title: Text(s.internet_stuff_text)),
            //ListTile(title: Text(s.nation_text)),
            ListTile(
                title: Text(s.number_text),
                onTap: () {
                  navigatorPush(context, NumberScreen());
                }),
            ListTile(title: Text(s.phone_number_text),
                onTap: () {
                  snackBarInfo(context, s);
                }),
            //ListTile(title: Text(s.placeholder_text)),
            //ListTile(title: Text(s.restaurant_text)),
          ],
        ),
      ),
    ],
  );
}

void navigatorPush(BuildContext context, Widget page) {
  Navigator.of(context)
      .push(MaterialPageRoute(builder: (BuildContext context) => page));
}

void snackBarInfo(BuildContext context, S s) {
  final snackBar = SnackBar(
    content: Text(
      s.empty_page,
      style: Theme.of(context).textTheme.bodyText1,
    ),
    backgroundColor: Theme.of(context).primaryColor,
    shape: RoundedRectangleBorder(),
  );
  Navigator.of(context).pop();
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
