// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a ru locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'ru';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "address_text": MessageLookupByLibrary.simpleMessage("Адрес"),
        "app_description":
            MessageLookupByLibrary.simpleMessage("Случайности не случайны..."),
        "app_name": MessageLookupByLibrary.simpleMessage("Random Things"),
        "app_text": MessageLookupByLibrary.simpleMessage("Приложение"),
        "appliance_text": MessageLookupByLibrary.simpleMessage("Прибор"),
        "bank_text": MessageLookupByLibrary.simpleMessage("Банк"),
        "beer_text": MessageLookupByLibrary.simpleMessage("Пиво"),
        "blood_text": MessageLookupByLibrary.simpleMessage("Кровь"),
        "business_credit_card_text":
            MessageLookupByLibrary.simpleMessage("Кредитная карта"),
        "categories_text": MessageLookupByLibrary.simpleMessage("Категории"),
        "code_text": MessageLookupByLibrary.simpleMessage("Код"),
        "coffee_text": MessageLookupByLibrary.simpleMessage("Кофе"),
        "color_text": MessageLookupByLibrary.simpleMessage("Цвет"),
        "commerce_text": MessageLookupByLibrary.simpleMessage("Коммерция"),
        "company_text": MessageLookupByLibrary.simpleMessage("Компания"),
        "computer_text": MessageLookupByLibrary.simpleMessage("Компьютер"),
        "crypto_coin_text":
            MessageLookupByLibrary.simpleMessage("Криптовалюта"),
        "crypto_text": MessageLookupByLibrary.simpleMessage("Крипто"),
        "dessert_text": MessageLookupByLibrary.simpleMessage("Десерт"),
        "device_text": MessageLookupByLibrary.simpleMessage("Устройство"),
        "empty_page": MessageLookupByLibrary.simpleMessage("Этого пока нет..."),
        "food_text": MessageLookupByLibrary.simpleMessage("Еда"),
        "id_number_text":
            MessageLookupByLibrary.simpleMessage("Номер индентификации"),
        "info_decimal_text":
            MessageLookupByLibrary.simpleMessage("Десятичное число"),
        "info_digit_text": MessageLookupByLibrary.simpleMessage("Цифра"),
        "info_first_text": MessageLookupByLibrary.simpleMessage(
            "Нажмите на кнопку внизу чтобы получить сгенерированный результат"),
        "info_leading_zero_number_text":
            MessageLookupByLibrary.simpleMessage("Число с нулём в начале"),
        "info_negative_number_text":
            MessageLookupByLibrary.simpleMessage("Негативное число"),
        "info_non_zero_number_text":
            MessageLookupByLibrary.simpleMessage("Число без нуля"),
        "info_number_text": MessageLookupByLibrary.simpleMessage("Число"),
        "info_positive_number_text":
            MessageLookupByLibrary.simpleMessage("Положительное число"),
        "internet_stuff_text":
            MessageLookupByLibrary.simpleMessage("Интернет вещь"),
        "name_text": MessageLookupByLibrary.simpleMessage("Имя"),
        "nation_text": MessageLookupByLibrary.simpleMessage("Нация"),
        "number_text": MessageLookupByLibrary.simpleMessage("Число"),
        "phone_number_text":
            MessageLookupByLibrary.simpleMessage("Номер телефона"),
        "placeholder_text": MessageLookupByLibrary.simpleMessage("Заполнитель"),
        "restaurant_text": MessageLookupByLibrary.simpleMessage("Ресторан"),
        "subscription_text": MessageLookupByLibrary.simpleMessage("Подписка"),
        "users_text": MessageLookupByLibrary.simpleMessage("Пользователи"),
        "vehicle_text": MessageLookupByLibrary.simpleMessage("Техника"),
        "welcome_text": MessageLookupByLibrary.simpleMessage(
            "Добро пожаловать в Random Things!")
      };
}
