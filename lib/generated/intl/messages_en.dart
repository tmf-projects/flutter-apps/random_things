// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "address_text": MessageLookupByLibrary.simpleMessage("Address"),
        "app_description":
            MessageLookupByLibrary.simpleMessage("Accidents are not random..."),
        "app_name": MessageLookupByLibrary.simpleMessage("Random Things"),
        "app_text": MessageLookupByLibrary.simpleMessage("App"),
        "appliance_text": MessageLookupByLibrary.simpleMessage("Appliance"),
        "bank_text": MessageLookupByLibrary.simpleMessage("Bank"),
        "beer_text": MessageLookupByLibrary.simpleMessage("Beer"),
        "blood_text": MessageLookupByLibrary.simpleMessage("Blood"),
        "business_credit_card_text":
            MessageLookupByLibrary.simpleMessage("Business Credit Card"),
        "categories_text": MessageLookupByLibrary.simpleMessage("Categories"),
        "code_text": MessageLookupByLibrary.simpleMessage("Code"),
        "coffee_text": MessageLookupByLibrary.simpleMessage("Coffee"),
        "color_text": MessageLookupByLibrary.simpleMessage("Color"),
        "commerce_text": MessageLookupByLibrary.simpleMessage("Commerce"),
        "company_text": MessageLookupByLibrary.simpleMessage("Company"),
        "computer_text": MessageLookupByLibrary.simpleMessage("Computer"),
        "crypto_coin_text": MessageLookupByLibrary.simpleMessage("CryptoCoin"),
        "crypto_text": MessageLookupByLibrary.simpleMessage("Crypto"),
        "dessert_text": MessageLookupByLibrary.simpleMessage("Dessert"),
        "device_text": MessageLookupByLibrary.simpleMessage("Device"),
        "empty_page": MessageLookupByLibrary.simpleMessage(
            "This is not yet available..."),
        "food_text": MessageLookupByLibrary.simpleMessage("Food"),
        "id_number_text": MessageLookupByLibrary.simpleMessage("ID Number"),
        "info_decimal_text": MessageLookupByLibrary.simpleMessage("Decimal"),
        "info_digit_text": MessageLookupByLibrary.simpleMessage("Digit"),
        "info_first_text": MessageLookupByLibrary.simpleMessage(
            "Click on the button at the bottom to get the generated result"),
        "info_leading_zero_number_text":
            MessageLookupByLibrary.simpleMessage("Leading Zero Number"),
        "info_negative_number_text":
            MessageLookupByLibrary.simpleMessage("Negative Number"),
        "info_non_zero_number_text":
            MessageLookupByLibrary.simpleMessage("Non Zero Number"),
        "info_number_text": MessageLookupByLibrary.simpleMessage("Number"),
        "info_positive_number_text":
            MessageLookupByLibrary.simpleMessage("Positive Number"),
        "internet_stuff_text":
            MessageLookupByLibrary.simpleMessage("Internet Stuff"),
        "name_text": MessageLookupByLibrary.simpleMessage("Name"),
        "nation_text": MessageLookupByLibrary.simpleMessage("Nation"),
        "number_text": MessageLookupByLibrary.simpleMessage("Number"),
        "phone_number_text":
            MessageLookupByLibrary.simpleMessage("Phone Number"),
        "placeholder_text": MessageLookupByLibrary.simpleMessage("Placeholder"),
        "restaurant_text": MessageLookupByLibrary.simpleMessage("Restaurant"),
        "subscription_text":
            MessageLookupByLibrary.simpleMessage("Subscription"),
        "users_text": MessageLookupByLibrary.simpleMessage("Users"),
        "vehicle_text": MessageLookupByLibrary.simpleMessage("Vehicle"),
        "welcome_text":
            MessageLookupByLibrary.simpleMessage("Welcome to Random Things!")
      };
}
