// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Random Things`
  String get app_name {
    return Intl.message(
      'Random Things',
      name: 'app_name',
      desc: '',
      args: [],
    );
  }

  /// `Accidents are not random...`
  String get app_description {
    return Intl.message(
      'Accidents are not random...',
      name: 'app_description',
      desc: '',
      args: [],
    );
  }

  /// `This is not yet available...`
  String get empty_page {
    return Intl.message(
      'This is not yet available...',
      name: 'empty_page',
      desc: '',
      args: [],
    );
  }

  /// `Click on the button at the bottom to get the generated result`
  String get info_first_text {
    return Intl.message(
      'Click on the button at the bottom to get the generated result',
      name: 'info_first_text',
      desc: '',
      args: [],
    );
  }

  /// `Welcome to Random Things!`
  String get welcome_text {
    return Intl.message(
      'Welcome to Random Things!',
      name: 'welcome_text',
      desc: '',
      args: [],
    );
  }

  /// `Categories`
  String get categories_text {
    return Intl.message(
      'Categories',
      name: 'categories_text',
      desc: '',
      args: [],
    );
  }

  /// `Address`
  String get address_text {
    return Intl.message(
      'Address',
      name: 'address_text',
      desc: '',
      args: [],
    );
  }

  /// `Appliance`
  String get appliance_text {
    return Intl.message(
      'Appliance',
      name: 'appliance_text',
      desc: '',
      args: [],
    );
  }

  /// `App`
  String get app_text {
    return Intl.message(
      'App',
      name: 'app_text',
      desc: '',
      args: [],
    );
  }

  /// `Bank`
  String get bank_text {
    return Intl.message(
      'Bank',
      name: 'bank_text',
      desc: '',
      args: [],
    );
  }

  /// `Beer`
  String get beer_text {
    return Intl.message(
      'Beer',
      name: 'beer_text',
      desc: '',
      args: [],
    );
  }

  /// `Blood`
  String get blood_text {
    return Intl.message(
      'Blood',
      name: 'blood_text',
      desc: '',
      args: [],
    );
  }

  /// `Business Credit Card`
  String get business_credit_card_text {
    return Intl.message(
      'Business Credit Card',
      name: 'business_credit_card_text',
      desc: '',
      args: [],
    );
  }

  /// `Code`
  String get code_text {
    return Intl.message(
      'Code',
      name: 'code_text',
      desc: '',
      args: [],
    );
  }

  /// `Coffee`
  String get coffee_text {
    return Intl.message(
      'Coffee',
      name: 'coffee_text',
      desc: '',
      args: [],
    );
  }

  /// `Commerce`
  String get commerce_text {
    return Intl.message(
      'Commerce',
      name: 'commerce_text',
      desc: '',
      args: [],
    );
  }

  /// `Company`
  String get company_text {
    return Intl.message(
      'Company',
      name: 'company_text',
      desc: '',
      args: [],
    );
  }

  /// `Computer`
  String get computer_text {
    return Intl.message(
      'Computer',
      name: 'computer_text',
      desc: '',
      args: [],
    );
  }

  /// `Crypto`
  String get crypto_text {
    return Intl.message(
      'Crypto',
      name: 'crypto_text',
      desc: '',
      args: [],
    );
  }

  /// `CryptoCoin`
  String get crypto_coin_text {
    return Intl.message(
      'CryptoCoin',
      name: 'crypto_coin_text',
      desc: '',
      args: [],
    );
  }

  /// `Color`
  String get color_text {
    return Intl.message(
      'Color',
      name: 'color_text',
      desc: '',
      args: [],
    );
  }

  /// `Dessert`
  String get dessert_text {
    return Intl.message(
      'Dessert',
      name: 'dessert_text',
      desc: '',
      args: [],
    );
  }

  /// `Device`
  String get device_text {
    return Intl.message(
      'Device',
      name: 'device_text',
      desc: '',
      args: [],
    );
  }

  /// `Food`
  String get food_text {
    return Intl.message(
      'Food',
      name: 'food_text',
      desc: '',
      args: [],
    );
  }

  /// `Name`
  String get name_text {
    return Intl.message(
      'Name',
      name: 'name_text',
      desc: '',
      args: [],
    );
  }

  /// `Users`
  String get users_text {
    return Intl.message(
      'Users',
      name: 'users_text',
      desc: '',
      args: [],
    );
  }

  /// `Subscription`
  String get subscription_text {
    return Intl.message(
      'Subscription',
      name: 'subscription_text',
      desc: '',
      args: [],
    );
  }

  /// `Vehicle`
  String get vehicle_text {
    return Intl.message(
      'Vehicle',
      name: 'vehicle_text',
      desc: '',
      args: [],
    );
  }

  /// `ID Number`
  String get id_number_text {
    return Intl.message(
      'ID Number',
      name: 'id_number_text',
      desc: '',
      args: [],
    );
  }

  /// `Internet Stuff`
  String get internet_stuff_text {
    return Intl.message(
      'Internet Stuff',
      name: 'internet_stuff_text',
      desc: '',
      args: [],
    );
  }

  /// `Nation`
  String get nation_text {
    return Intl.message(
      'Nation',
      name: 'nation_text',
      desc: '',
      args: [],
    );
  }

  /// `Number`
  String get number_text {
    return Intl.message(
      'Number',
      name: 'number_text',
      desc: '',
      args: [],
    );
  }

  /// `Phone Number`
  String get phone_number_text {
    return Intl.message(
      'Phone Number',
      name: 'phone_number_text',
      desc: '',
      args: [],
    );
  }

  /// `Placeholder`
  String get placeholder_text {
    return Intl.message(
      'Placeholder',
      name: 'placeholder_text',
      desc: '',
      args: [],
    );
  }

  /// `Restaurant`
  String get restaurant_text {
    return Intl.message(
      'Restaurant',
      name: 'restaurant_text',
      desc: '',
      args: [],
    );
  }

  /// `Number`
  String get info_number_text {
    return Intl.message(
      'Number',
      name: 'info_number_text',
      desc: '',
      args: [],
    );
  }

  /// `Leading Zero Number`
  String get info_leading_zero_number_text {
    return Intl.message(
      'Leading Zero Number',
      name: 'info_leading_zero_number_text',
      desc: '',
      args: [],
    );
  }

  /// `Decimal`
  String get info_decimal_text {
    return Intl.message(
      'Decimal',
      name: 'info_decimal_text',
      desc: '',
      args: [],
    );
  }

  /// `Positive Number`
  String get info_positive_number_text {
    return Intl.message(
      'Positive Number',
      name: 'info_positive_number_text',
      desc: '',
      args: [],
    );
  }

  /// `Negative Number`
  String get info_negative_number_text {
    return Intl.message(
      'Negative Number',
      name: 'info_negative_number_text',
      desc: '',
      args: [],
    );
  }

  /// `Non Zero Number`
  String get info_non_zero_number_text {
    return Intl.message(
      'Non Zero Number',
      name: 'info_non_zero_number_text',
      desc: '',
      args: [],
    );
  }

  /// `Digit`
  String get info_digit_text {
    return Intl.message(
      'Digit',
      name: 'info_digit_text',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'ru'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
