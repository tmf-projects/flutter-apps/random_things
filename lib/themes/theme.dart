import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final ThemeData themeLight = ThemeData.light().copyWith(
  brightness: Brightness.dark,
  primaryColor: Colors.blueAccent,
  scaffoldBackgroundColor: Colors.white,
  buttonTheme: ButtonThemeData(buttonColor: Colors.blueAccent),
  elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.blueAccent))),
  textTheme: TextTheme(
    headline1: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 96,
        fontWeight: FontWeight.w300,
        letterSpacing: -1.5),
    headline2: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 60,
        fontWeight: FontWeight.w300,
        letterSpacing: -0.5),
    headline3: GoogleFonts.rubik(
        color: Colors.black, fontSize: 48, fontWeight: FontWeight.w400),
    headline4: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 34,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25),
    headline5: GoogleFonts.rubik(
        color: Colors.black, fontSize: 24, fontWeight: FontWeight.w400),
    headline6: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 20,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.15),
    subtitle1: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.15),
    subtitle2: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 14,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.1),
    bodyText1: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.5),
    bodyText2: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 14,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25),
    button: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 14,
        fontWeight: FontWeight.w500,
        letterSpacing: 1.25),
    caption: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 12,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.4),
    overline: GoogleFonts.rubik(
        color: Colors.black,
        fontSize: 10,
        fontWeight: FontWeight.w400,
        letterSpacing: 1.5),
  ),
);

final ThemeData themeDark = ThemeData.dark().copyWith(
  brightness: Brightness.dark,
  primaryColor: Colors.indigo,
  scaffoldBackgroundColor: Colors.grey[900],
  buttonTheme: ButtonThemeData(buttonColor: Colors.indigo),
  elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.indigo))),
  textTheme: TextTheme(
    headline1: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 96,
        fontWeight: FontWeight.w300,
        letterSpacing: -1.5),
    headline2: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 60,
        fontWeight: FontWeight.w300,
        letterSpacing: -0.5),
    headline3: GoogleFonts.rubik(
        color: Colors.white, fontSize: 48, fontWeight: FontWeight.w400),
    headline4: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 34,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25),
    headline5: GoogleFonts.rubik(
        color: Colors.white, fontSize: 24, fontWeight: FontWeight.w400),
    headline6: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 20,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.15),
    subtitle1: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.15),
    subtitle2: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 14,
        fontWeight: FontWeight.w500,
        letterSpacing: 0.1),
    bodyText1: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.5),
    bodyText2: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 14,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.25),
    button: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 14,
        fontWeight: FontWeight.w500,
        letterSpacing: 1.25),
    caption: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 12,
        fontWeight: FontWeight.w400,
        letterSpacing: 0.4),
    overline: GoogleFonts.rubik(
        color: Colors.white,
        fontSize: 10,
        fontWeight: FontWeight.w400,
        letterSpacing: 1.5),
  ),
);
