import 'package:flutter/material.dart';
import 'package:random_things/generated/l10n.dart';
import 'package:random_things/services/numbers_service.dart';

class NumberScreen extends StatefulWidget {
  const NumberScreen({Key? key}) : super(key: key);

  @override
  _NumberScreenState createState() => _NumberScreenState();
}

class _NumberScreenState extends State<NumberScreen> {
  NumbersService service = NumbersService.filled();
  Map<String, dynamic>? map;
  int? number;
  double? decimal;
  double? positiveNumber;
  double? negativeNumber;
  int? nonZeroNumber;
  int? digit;

  @override
  void initState() {
    super.initState();
    _getMap();
  }

  void _getMap() async {
    map = await service.getNumber();
    _setValues();
  }

  void _setValues() {
    number = map?['number'];
    decimal = map?['decimal'];
    positiveNumber = map?['positive'];
    negativeNumber = map?['negative'];
    nonZeroNumber = map?['non_zero_number'];
    digit = map?['digit'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).number_text),
        centerTitle: true,
        elevation: 0,
      ),
      body: Center(
        child: (number == null)
            ? Container(
                width: 290,
                height: 100,
                padding: EdgeInsets.all(10),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Colors.transparent),
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    color: Theme.of(context).primaryColor,
                    boxShadow: [
                      BoxShadow(
                          color: Color(Theme.of(context).primaryColor.value)
                              .withOpacity(0.2),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 0))
                    ]),
                child: Text(S.of(context).info_first_text,
                    style: Theme.of(context).textTheme.bodyText1,
                    textAlign: TextAlign.center),
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(S.of(context).info_number_text,
                      style: Theme.of(context).textTheme.headline6),
                  Text(number.toString(),
                      style: Theme.of(context).textTheme.bodyText1),
                  SizedBox(height: 10),
                  Text(S.of(context).info_decimal_text,
                      style: Theme.of(context).textTheme.headline6),
                  Text(decimal.toString(),
                      style: Theme.of(context).textTheme.bodyText1),
                  SizedBox(height: 10),
                  Text(S.of(context).info_positive_number_text,
                      style: Theme.of(context).textTheme.headline6),
                  Text(positiveNumber.toString(),
                      style: Theme.of(context).textTheme.bodyText1),
                  SizedBox(height: 10),
                  Text(S.of(context).info_negative_number_text,
                      style: Theme.of(context).textTheme.headline6),
                  Text(negativeNumber.toString(),
                      style: Theme.of(context).textTheme.bodyText1),
                  SizedBox(height: 10),
                  Text(S.of(context).info_non_zero_number_text,
                      style: Theme.of(context).textTheme.headline6),
                  Text(nonZeroNumber.toString(),
                      style: Theme.of(context).textTheme.bodyText1),
                  SizedBox(height: 10),
                  Text(S.of(context).info_digit_text,
                      style: Theme.of(context).textTheme.headline6),
                  Text(digit.toString(),
                      style: Theme.of(context).textTheme.bodyText1),
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            _getMap();
          });
        },
        child: Icon(Icons.restart_alt_rounded),
        backgroundColor: Theme.of(context).primaryColor,
        foregroundColor: Theme.of(context).scaffoldBackgroundColor,
      ),
    );
  }
}
